import React from 'react';
import ReactDOM from 'react-dom';
import App from './components/App';
import * as firebase from 'firebase';
import registerServiceWorker from './registerServiceWorker';

var config = {
    apiKey: "AIzaSyC7r8vhSCtoYYnsugfj3oH0OHCBA6iDubg",
    authDomain: "react-cloud-6c5f6.firebaseapp.com",
    databaseURL: "https://react-cloud-6c5f6.firebaseio.com",
    projectId: "react-cloud-6c5f6",
    storageBucket: "react-cloud-6c5f6.appspot.com",
    messagingSenderId: "808187769451"
  };
  firebase.initializeApp(config);

ReactDOM.render(<App />, document.getElementById('root'));
registerServiceWorker();
